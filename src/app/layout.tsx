'use client'
import { Box, AppBar, Toolbar, Button, Typography } from '@mui/material'
import  EmojiEventsIcon from '@mui/icons-material/EmojiEvents';
import { UserProvider } from '@auth0/nextjs-auth0/client';

import Header from './header';

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html>
      <head>
        <link rel="preconnect" href="https://fonts.googleapis.com"/>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin=''/>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet"/>
      </head>
      <UserProvider>
        <body style={{
          overflow: "auto",
          margin:0,
          fontFamily: "Roboto"
        }}>
          <Box>
            <Header/>
            <Box px={30}>
              {children}
            </Box>
          </Box>
        </body>
      </UserProvider>
    </html>
  )
}