import { NextApiRequest, NextApiResponse } from 'next';
import { NextResponse } from 'next/server';

import conn from '@/lib/db'
import { getSession } from '@auth0/nextjs-auth0';

//get id from url


export async function GET(req: Request, { params }: { params: { id: number } }){

    try {

        const query = `SELECT * FROM lab1.susreti WHERE natjecanje_id=`+params.id+` order by kolo;`
        
        const result = await conn.query(query);

        return NextResponse.json(result.rows);
    }
    catch(error)
    {
        console.log(error)
        return NextResponse.json({message:"Error caught"+error},{status:500});
    }
}

export async function PATCH(req: Request, { params }: { params: { id: number } }){

        try{

            const session = await getSession();
            if (!session) {
                return NextResponse.redirect('/api/auth/login');
            }

            const data = await req.json();

            const query = `UPDATE lab1.susreti SET rezultat_1=$1,rezultat_2=$2,odigrano=true WHERE id=$3;`
            const values = [data.rezultat_1,data.rezultat_2, data.id]
    
            const result = await conn.query(query, values);
            return NextResponse.json(result.rows);
        }
        catch(error)
        {
            console.log(error)
            return NextResponse.json({message:"Error caught"+error},{status:500});
        }
}