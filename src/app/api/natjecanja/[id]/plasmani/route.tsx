import { NextApiRequest, NextApiResponse } from 'next';
import { NextResponse } from 'next/server';

import conn from '@/lib/db'
import { getSession } from '@auth0/nextjs-auth0';

//get id from url


type RowDataPlasman = {
    plasman: number;
    natjecatelj: string;
    broj_poraza: number;
    broj_remija: number;
    broj_pobjeda: number;
    bodovi: number;
}

export async function GET(req: Request, { params }: { params: { id: number } }){

    try {
        
        const query1 = `SELECT * FROM lab1.susreti WHERE natjecanje_id=`+params.id+` order by kolo;`
        const query1Results = await conn.query(query1);

        const query2 = `SELECT *, array_to_string(n.sustav_bodovanja,'/','0') as sustav_bodovanja_Str, array_length(n.natjecatelji,1) as broj_natjecatelja FROM lab1.natjecanja n
        WHERE id=`+params.id+`;`
        const query2Results = await conn.query(query2);

        const susreti = query1Results.rows;
        const natjecanje = query2Results.rows[0];

        const sustav_bodovanja = natjecanje.sustav_bodovanja;
        const natjecatelji: string[] = natjecanje.natjecatelji;

        // calculate the points for each player

        var plasmaniUnsorted: {[key: string]: {bodovi: number,broj_pobjeda: number, broj_remija: number, broj_poraza: number}} = {};

        natjecatelji.map((natjecatelj) => {

            plasmaniUnsorted[natjecatelj] = {
                bodovi: 0,
                broj_pobjeda: 0,
                broj_remija: 0,
                broj_poraza: 0
            }
        });

        susreti.map((susret) => {
            if (susret.odigrano == false) {
                return;
            }
            if (susret.rezultat_1 > susret.rezultat_2) {
                plasmaniUnsorted[susret.natjecatelj_1].bodovi += sustav_bodovanja[0];
                plasmaniUnsorted[susret.natjecatelj_2].bodovi += sustav_bodovanja[2];
                plasmaniUnsorted[susret.natjecatelj_1].broj_pobjeda += 1;
                plasmaniUnsorted[susret.natjecatelj_2].broj_poraza += 1;
            } else if (susret.rezultat_1 < susret.rezultat_2) {
                plasmaniUnsorted[susret.natjecatelj_1].bodovi += sustav_bodovanja[2];
                plasmaniUnsorted[susret.natjecatelj_2].bodovi += sustav_bodovanja[0];
                plasmaniUnsorted[susret.natjecatelj_2].broj_pobjeda += 1;
                plasmaniUnsorted[susret.natjecatelj_1].broj_poraza += 1;
            } else {
                plasmaniUnsorted[susret.natjecatelj_1].bodovi += sustav_bodovanja[1];
                plasmaniUnsorted[susret.natjecatelj_2].bodovi += sustav_bodovanja[1];
                plasmaniUnsorted[susret.natjecatelj_1].broj_remija += 1;
                plasmaniUnsorted[susret.natjecatelj_2].broj_remija += 1;
            }
        }
        );

        // sort the players by points

        const sorted_plasmani = Object.entries(plasmaniUnsorted).sort((a, b) => {
            if (b[1].bodovi == a[1].bodovi) {
                if (b[1].broj_pobjeda == a[1].broj_pobjeda) {
                    if (b[1].broj_remija == a[1].broj_remija) {
                        return a[1].broj_poraza - b[1].broj_poraza
                    }
                    return b[1].broj_remija - a[1].broj_remija
                }
                return b[1].broj_pobjeda - a[1].broj_pobjeda
            }
            return b[1].bodovi - a[1].bodovi
        });

        // PARSE TO RowDataPlasman

        var plasmani: RowDataPlasman[] = [];

        sorted_plasmani.map((plasman, index) => {
            plasmani.push({
                plasman: index + 1,
                natjecatelj: plasman[0],
                broj_poraza: plasman[1].broj_poraza,
                broj_remija: plasman[1].broj_remija,
                broj_pobjeda: plasman[1].broj_pobjeda,
                bodovi: plasman[1].bodovi
            })
        });

        return NextResponse.json(plasmani);
    }
    catch(error)
    {
        console.log(error)
        return NextResponse.json({message:"Error caught"+error},{status:500});;
    }
}

