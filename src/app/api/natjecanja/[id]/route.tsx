import { NextApiRequest, NextApiResponse } from 'next';
import { NextResponse } from 'next/server';

import conn from '../../../../lib/db'

//get id from url


export async function GET(req: Request, { params }: { params: { id: number } }){

    try {

        const query = `SELECT *, array_to_string(n.sustav_bodovanja,'/','0') as sustav_bodovanja_Str, array_length(n.natjecatelji,1) as broj_natjecatelja FROM lab1.natjecanja n
        WHERE id=`+params.id+`;`
        
        const result = await conn.query(query);

        return NextResponse.json(result.rows);
    }
    catch(error)
    {
        console.log(error)
        return NextResponse.json({message:"Error caught"+error},{status:500});
    }
}

