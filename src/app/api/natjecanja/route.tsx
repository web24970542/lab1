import { NextApiRequest, NextApiResponse } from 'next';
import { NextResponse } from 'next/server';


import conn from '../../../lib/db'
import { getSession } from '@auth0/nextjs-auth0';

const ROUND_ROBIN_CONFIG = {
    4: {
        1: [[0, 2], [1, 3]],
        2: [[0, 1], [2, 3]],
        3: [[0, 3], [1, 2]],
    },
    5: {
        1: [[0, 3], [1, 4], [2, -1]],
        2: [[0, 1], [2, 3], [4, -1]],
        3: [[0, 2], [3, 4], [1, -1]],
        4: [[4, 2], [3, 1], [0, -1]],
        5: [[4, 0], [1, 2], [3, -1]],
    },
    6: {
        1: [[0,5],[1,4],[2,3]],
        2: [[0,4],[5,3],[1,2]],
        3: [[0,3],[4,2],[5,1]],
        4: [[0,2],[3,1],[4,5]],
        5: [[0,1],[2,5],[3,4]]
    },
    7:{
        1: [[0,6],[1,5],[2,4],[3,-1]],
        2: [[0,5],[6,4],[1,3],[2,-1]],
        3: [[0,4],[5,3],[6,2],[1,-1]],
        4: [[0,3],[4,2],[5,1],[6,-1]],
        5: [[0,2],[3,1],[4,6],[5,-1]],
        6: [[0,1],[2,6],[3,5],[4,-1]],
        7: [[0,-1],[1,6],[2,5],[3,4]]
    },
    8:{
        1: [[0,7],[1,6],[2,5],[3,4]],
        2: [[0,6],[7,5],[1,4],[2,3]],
        3: [[0,5],[6,4],[7,3],[1,2]],
        4: [[0,4],[5,3],[6,2],[7,1]],
        5: [[0,3],[4,2],[5,1],[6,7]],
        6: [[0,2],[3,1],[4,7],[5,6]],
        7: [[0,1],[2,7],[3,6],[4,5]]
    }
}



export async function GET(req: Request){

    try {

        const session = await getSession();
        if (!session) {
            return NextResponse.redirect('/api/auth/login');
        }

        const userId = session.user?.sub;
       
        const query = `SELECT *, array_to_string(n.sustav_bodovanja,'/','0') as sustav_bodovanja_Str, array_length(n.natjecatelji,1) as broj_natjecatelja FROM lab1.natjecanja n WHERE user_id='${userId}';`
        
        const result = await conn.query(query);

        return NextResponse.json(result.rows);
    }
    catch(error)
    {
        console.log(error)
        return NextResponse.json({message:"Error caught"+error},{status:500});
    }
}

const generirajSusrete = (natjecatelji: string[], natjecanjeId: number) =>{

    const brojNatjecatelja = natjecatelji.length;

    //@ts-ignore
    const config = ROUND_ROBIN_CONFIG[brojNatjecatelja];

    for (let i = 1; i <= Object.keys(config).length; i++) {
        const round = config[i];
        for (let j = 0; j < round.length; j++) {
    
            const match = round[j];
            const home = match[0];
            const away = match[1];
            if (away === -1) continue;
            //susreti.push([natjecatelji[home], natjecatelji[away]]);
            const query2 = `INSERT INTO lab1.susreti (natjecanje_id, kolo, natjecatelj_1, natjecatelj_2, rezultat_1, rezultat_2) VALUES (${natjecanjeId}, ${i}, '${natjecatelji[home]}', '${natjecatelji[away]}', 0, 0);`;
            conn.query(query2);
    
        }
    }
}

export async function POST(req: Request) {

    try {

        const session = await getSession();
        if (!session) {
            return NextResponse.redirect('/api/auth/login');
        }

        const userId = session.user?.sub;

        // Parse the request body as JSON
        const data = await req.json();

        if (!data) {
            return NextResponse.json("Error, missing data");
        } else {
            const { naziv, natjecatelji, sustav_bodovanja } = data;

            const query = `INSERT INTO lab1.natjecanja (naziv, natjecatelji, sustav_bodovanja, user_id) 
            VALUES ('${naziv}', '{${(natjecatelji as string[]).join(",")}}', '{${(sustav_bodovanja as number[]).join(",")}}','${userId}')
             RETURNING id;`;
           
            const result = await conn.query(query).then(
                (res) => {
                    generirajSusrete(natjecatelji, res.rows[0].id);
                    return  res.rows[0].id;
                }
            ) as number;


            return NextResponse.json(result);
        }
    } catch (error) {
        console.log(error);
        return NextResponse.json({message:"Error caught"+error},{status:500});
    }
}
