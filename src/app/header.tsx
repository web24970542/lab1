import { Box, AppBar, Toolbar, Button, Typography, CircularProgress } from '@mui/material'
import  EmojiEventsIcon from '@mui/icons-material/EmojiEvents';

import { useUser } from '@auth0/nextjs-auth0/client';

const Header = () =>{

    const { user, error, isLoading } = useUser();

    return(

        <AppBar  position="static">      
            <Toolbar sx={{display: "flex", justifyContent:"space-between", color:"white"}}>
            <Box display={"flex"} alignItems={"center"} >
                <EmojiEventsIcon fontSize='large'/>
                <a style={{color:"white",textDecoration:"none"}} href='/'><Typography ml={2} variant='h5'>Praćenje natjecanja</Typography></a>
            </Box>
            { user===undefined ?
                <Box>
                    <Button variant='outlined' onClick={(evt) =>{
                        window.location.href = "/api/auth/login"
                    }} color="inherit">Prijavi se</Button> 
                </Box> :
                <Box display={"flec"} flexDirection={"row"} alignItems={"center"}>
                    {user.email ? <Typography mr={2}>Pozdrav, {user.email}!</Typography> : <></>}
                    <Button variant='outlined' onClick={(evt) =>{
                        window.location.href = "/api/auth/logout"
                    }} color="inherit">Odjavi se</Button>
                </Box>
            }
            </Toolbar>
        </AppBar> 
    )
}

export default Header;