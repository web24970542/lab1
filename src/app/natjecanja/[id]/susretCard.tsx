import {Box, Button, Card, Input, Typography} from '@mui/material';
import { useState } from 'react';

import axios from 'axios';


type RowDataSusret = {
    id: number;
    kolo: number;
    natjecatelj_1: string;
    natjecatelj_2: string;
    rezultat_1: number;
    rezultat_2: number;
    odigrano: boolean;
}

const SusretCard = (props: {susret: RowDataSusret, refreshPlasmani: () => void}) => {

    const [rez1, setRez1] = useState(props.susret.rezultat_1);
    const [rez2, setRez2] = useState(props.susret.rezultat_2);

    const [oldRez1, setOldRez1] = useState(props.susret.rezultat_1);
    const [oldRez2, setOldRez2] = useState(props.susret.rezultat_2);

    const [odigran,setOdigran] = useState(props.susret.odigrano);

    const {refreshPlasmani} = props;


    // patch new rezultat

    const patchRezultat = () => {

        axios.patch(`/api/natjecanja/${props.susret.kolo}/susreti`, {
            id: props.susret.id,
            rezultat_1: rez1,
            rezultat_2: rez2
        }).then((res) => {
            setOldRez1(rez1);
            setOldRez2(rez2);
            refreshPlasmani();
        }).catch((error) => {
            console.log(error);
        })

        //refresh susreti in parent

    }

    const { susret} = props;

    return (

        <Card sx={{bgcolor: "#eeeeee", margin: 2, padding: 2}} >
            <h3>{susret.natjecatelj_1}</h3> vs <h3>{susret.natjecatelj_2}</h3>
            <Box display={"flex"} flexDirection={"row"}>
                <Input type="number" value={rez1}
                sx={{
                    bgcolor: "#dddddd",
                    px:2
                }}
                onChange={(e) =>  {
                    if (parseInt(e.target.value)<0) return;
                        setRez1(parseInt(e.target.value))
                }}
                />
                <Typography variant='h4' mx={2}>:</Typography>
                <Input type="number" value={rez2}
                sx={{
                    bgcolor: "#dddddd",
                    px:2
                }}
                onChange={(e) =>  {
                    if (parseInt(e.target.value)<0) return;
                        setRez2(parseInt(e.target.value))
                }}
                />
            </Box>
                {odigran ? <Typography color={"#42c728"} variant='body1'>Susret odigran</Typography> : <Typography color={"#536ffc"} variant='body1'>Susret nije još odigran</Typography>}

            <Button disabled={
                rez1===oldRez1 && rez2===oldRez2
            } sx={{mt:2}} variant='contained' 
            onClick={() =>{
                patchRezultat();
                setOdigran(true);
            }}
            >Spremi rezultat</Button>

        </Card>
    );
        
}

export default SusretCard;
