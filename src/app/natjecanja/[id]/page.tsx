"use client"
import { Alert, Box, CircularProgress, Grid, List, ListItem, ListItemText, Paper, Typography } from "@mui/material" 
import { Button, Snackbar } from '@mui/material'
import { useEffect, useState } from "react"

import { useUser } from "@auth0/nextjs-auth0/client";

import DataTable from 'react-data-table-component';

import SusretCard from "./susretCard";

type RowDataNatjecanje = {
    id: number;
    naziv: string;
    natjecatelji: string[];
    sustav_bodovanja: number[];
    sustav_bodovanja_str: string;
    broj_natjecatelja: number;
  }

type RowDataPlasman = {
    plasman: number;
    natjecatelj: string;
    broj_poraza: number;
    broj_remija: number;
    broj_pobjeda: number;
    bodovi: number;
}

type plasmani =
{
    [key: string]: {bodovi: number,broj_pobjeda: number, broj_remija: number, broj_poraza: number}
};

type RowDataSusret = {
    id: number;
    kolo: number;
    natjecatelj_1: string;
    natjecatelj_2: string;
    rezultat_1: number;
    rezultat_2: number;
    odigrano: false;
}

const columns = [
    {
        name: 'Plasman',
        selector: (row: RowDataPlasman) => row.plasman,
        sortable: true,
    },
    {
        name: 'Natjecatelj',
        selector: (row: RowDataPlasman) => row.natjecatelj,
        sortable: true,
    },
    {
        name: 'Broj bodova',
        selector: (row: RowDataPlasman) => row.bodovi,
    },
    {
        name: 'Broj pobjeda',
        selector: (row: RowDataPlasman) => row.broj_pobjeda,
    },
    {
        name: 'Broj remija',
        selector: (row: RowDataPlasman) => row.broj_remija,
    },
    {
        name: 'Broj poraza',
        selector: (row: RowDataPlasman) => row.broj_poraza,
    },
    
  ];

const PregledNatjecanja = ({ params }: { params: { id: number } }) =>{
    
    // if id isnt number
    if (params.id === undefined || params.id === null || params.id <= 0 ) {
        window.location.href = "/natjecanja"
        return null
    }

    // if it has any non numeric characters, redirect to /natjecanja
    if (!/^\d+$/.test(params.id.toString())) {
        window.location.href = "/natjecanja"
        return null
    }
    const [recordNatjecanje, setRecordNatjecanje] = useState<RowDataNatjecanje | undefined>()
    const [recordsSusreti, setRecordSusreti] = useState<RowDataSusret[]>([])
    const [recordsPlasmani, setRecordPlasmani] = useState<RowDataPlasman[]>([])
    const [loading, setLoading] = useState(true)
    const [maxKolo, setMaxKolo] = useState(0)

    const { user, error, isLoading } = useUser();

    const fetchNatjecanjeData = () =>{
        if (recordNatjecanje === undefined) {
            setRecordNatjecanje(undefined)
            fetch("/api/natjecanja/" + params.id).then((res) => {
                if (res.status !== 200) {
                    console.log("Error fetching data from /api/natjecanja/[id]")
                    setLoading(false)
                    return
                }
                res.json().then((data) => {
                    
                    if (data.length === 0) {
                        window.location.href = "/natjecanja"
                        return
                    }
                    setRecordNatjecanje(data[0])
                    setLoading(false)
                })
            }).catch((error) => {
                console.log(error)
                window.location.href = "/natjecanja"
            })
        }
    }

    const fetchSusretiData = () =>{
            fetch("/api/natjecanja/" + params.id+"/susreti").then((res) => {
                if (res.status !== 200) {
                    console.log("Error fetching data from /api/natjecanja/[id]/susreti")
                    setLoading(false)
                    return
                }
                res.json().then((data) => {
               
                    if (data.length === 0) {
                        return
                    }
                    setRecordSusreti(data)
                    setLoading(false)

                })
            }).catch((error) => {
                console.log(error)
                window.location.href = "/natjecanja"
            })
    }

    const fetchPlasmaniData = () =>{
            fetch("/api/natjecanja/" + params.id+"/plasmani").then((res) => {
                if (res.status !== 200) {
                    console.log("Error fetching data from /api/natjecanja/[id]/plasmani")
                    setLoading(false)
                    return
                }
                res.json().then((data) => {
                   
                    if (data.length === 0) {
                        return
                    }
                    setRecordPlasmani(data)
                    setLoading(false)
                })
            }).catch((error) => {
                console.log(error)
                window.location.href = "/natjecanja"
            })
    }
 
    useEffect(()=>{
         
        fetchNatjecanjeData();
        fetchSusretiData();
        fetchPlasmaniData();

    }, [])

    useEffect(()=>{
        if (recordsSusreti.length === 0) return;
        var maxKoloTemp = 0;
        recordsSusreti.map((susret) =>{
            if (susret.kolo > maxKoloTemp) maxKoloTemp = susret.kolo;
        })
        setMaxKolo(maxKoloTemp);
    },[recordsSusreti])

    return (
        <Box width={"100%"} height={"100%"}>
            <Typography variant="h3" mt={5} mb={5} textAlign={"center"}>Upravljanje natjecanjem</Typography>
            <Typography variant="h6" mt={5} mb={5} textAlign={"center"}>Podijeli pregled natjecanja s drugima:  <a target="_blank" href={"/"+params.id}>{window.location.host+"/"+params.id}</a></Typography>

            {loading ? <CircularProgress sx={{mt:5}}/> : 
                <Grid rowSpacing={10} columnSpacing={10} container pt={5} >
                    <Grid item xs={6}>
                        <Paper sx={{ bgcolor: "#71A3D5", p:2}} >
                            <Box display={"flex"} flexDirection={"column"}>
                                <Typography  variant="h5">Naziv natjecanja:</Typography> <Typography variant="h4" fontWeight={700}>{recordNatjecanje?.naziv}</Typography>
                                <Typography mt={2} variant="h5">Broj natjecatelja:</Typography> <Typography variant="h4" fontWeight={700}>{recordNatjecanje?.broj_natjecatelja}</Typography>
                                <Typography mt={2} variant="h5">Način bodovanja</Typography> <Typography variant="h4" fontWeight={700}>{recordNatjecanje?.sustav_bodovanja_str}</Typography>
                                <Typography variant="body1">(pobjeda/remi/poraz)</Typography>
                                {
                                user && user.email?
                                    <>
                                        <Typography mt={2} variant="h5">Organizator:</Typography>
                                        <Typography variant="h4" fontWeight={700}>{ user.email}</Typography> 
                                    </>
                                : <></>
                                }
                                <Typography mt={2} variant="h5">Natjecatelji:</Typography>
                                <List>
                                    {recordNatjecanje?.natjecatelji?.map((natjecatelj, index) => (
                                        <ListItem 
                                            key={"natjecatelj"+index+"-"+natjecatelj+"-li"} 
                                            style={{padding: 0}}>
                                            <ListItemText primary={natjecatelj} key={
                                                "natjecatelj"+index+"-"+natjecatelj+"-lit"} />
                                        </ListItem>
                                    ))}
                                </List>
                            </Box>
                        </Paper>
                    </Grid>

                    <Grid item xs={6}>
                        <DataTable
                            title="Plasmani"
                            columns={columns}
                            data={recordsPlasmani}
                        />
                    </Grid>
                  

                    <Grid item xs={12}  display={"flex"} flexDirection={"column"}>
                       { 
                         [...Array(maxKolo).keys() as any].map((kolo: number, index: number) => (

                            <Grid container key={"kolo"+index} display={"flex"} flexDirection={"column"}>
                                <Typography variant="h5">Kolo {index+1}</Typography>
                                <Box display={"flex"} flexDirection={"row"}>
                                    {recordsSusreti.filter((susret) => susret.kolo === index+1).map((susret, index) => (
                                        <Grid item xs={3}>
                                            <SusretCard refreshPlasmani={fetchPlasmaniData} key={"susret"+index} susret={susret}/>
                                        </Grid>
                                    ))}
                                </Box>
                            </Grid>

                            ))
                       }
                    </Grid>

                </Grid>
            }
        </Box>
    )
}
/*.map((susret, index) => (
    <Grid item xs={4}>
        <SusretCard key={"susret"+index} susret={susret}/>
    </Grid>
)*/
export default PregledNatjecanja