"use client"
import { useUser } from "@auth0/nextjs-auth0/client"
import { useEffect } from "react";

import {CircularProgress, Box} from "@mui/material"

export default function NatjecanjaLayout({
    children,
  }: {
    children: React.ReactNode
  }) {

    const {user,isLoading,error} = useUser();

    useEffect(() =>{
        if (isLoading===false)
        {
            if (!user)
                window.location.href = "/api/auth/login"
        }
    }, [isLoading])


    return (isLoading ? <Box display={"flex"} justifyContent={"center"} width={"100%"}><CircularProgress sx={{mt:5}}/></Box> : user ? <>{children}</> : <></>)
  }