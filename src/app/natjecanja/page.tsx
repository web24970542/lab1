"use client"
import { Box, Button, CircularProgress, Typography,TextField, FormLabel} from "@mui/material";
import WebAssetOffIcon from '@mui/icons-material/WebAssetOff';
import DataTable from 'react-data-table-component';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import { SyntheticEvent } from "react";

import axios  from "axios";

import { Suspense, useEffect, useState } from "react";

const columns = [
  {
      name: 'ID',
      selector: (row: RowData) => row.id,
      sortable: true,
  },
  {
      name: 'Naziv',
      selector: (row: RowData) => row.naziv,
      sortable: true,
  },
  {
      name: 'Broj natjecatelja',
      selector: (row: RowData) => row.broj_natjecatelja,
      sortable: true,
  },
  {
      name: 'Način bodovanja',
      selector: (row: RowData) => row.sustav_bodovanja_str,
  },
  {
      name: 'Natjecatelji',
      selector: (row: RowData) => row.natjecatelji.join(", "),
  },
];

type RowData = {
  id: number;
  naziv: string;
  natjecatelji: string[];
  sustav_bodovanja: number[];
  sustav_bodovanja_str: string;
  broj_natjecatelja: number;
}

export default function Home() {

  const [records, setRecords] = useState<RowData[]>([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [naziv, setNaziv] = useState("");
  const [natjecatelji, setNatjecatelji] = useState("");
  const [nacinBodovanja, setNacinBodovanja] = useState<{pobjeda: number, remi: number, poraz: number}>({pobjeda:0,remi:0,poraz:0});
  const [error, setError] = useState({naziv: false, natjecatelji: false, nacinBodovanja: {pobjeda: false, remi: false, poraz: false }});
  const [loading, setLoading] = useState(true);

  useEffect(()=>{
    if (records.length===0) {
      axios.get("/api/natjecanja").then( (res) =>{
        if (res.data.length===0) return;
        setRecords(res.data as RowData[]);
      }).catch((error) =>{
        console.log("error occured during get", error)
      })
      setLoading(false);
    }
  },[])

  const handleClose = () =>{
    setOpenDialog(false);
  }

  const handleOpen = () =>{
    setOpenDialog(true);
  }

  const validateErrors = () => {
    var errorTemp = {naziv: false, natjecatelji: false, nacinBodovanja: {pobjeda: false, remi: false, poraz: false}};
    var errorFound = false;


    if (natjecatelji.split("\n").length<4 ||  natjecatelji.split("\n").length>8){
      errorTemp.natjecatelji = true;
      errorFound = true;
    }

    if (naziv==="") {
      errorTemp.naziv = true;
      errorFound = true;
    }

    /*if (nacinBodovanja.pobjeda===0) {
      errorTemp.nacinBodovanja.pobjeda = true;
      errorFound = true;
    }
    
    if (nacinBodovanja.remi===0) {
      errorTemp.nacinBodovanja.remi = true;
      errorFound = true;
    }
    
    if (nacinBodovanja.poraz===0) {
      errorTemp.nacinBodovanja.poraz = true;
      errorFound = true;
    }*/

    setError(errorTemp);

    if (errorFound) {
      return true;
    }
    else {
      return false;
    } 
    
  }

  const handleConfirm = (event: SyntheticEvent) =>{

    if (validateErrors()) return;
  
    handleClose();

    axios.post("/api/natjecanja",{
      naziv: naziv,
      natjecatelji: natjecatelji.split("\n"),
      sustav_bodovanja: [nacinBodovanja.pobjeda, nacinBodovanja.remi, nacinBodovanja.poraz]
    },{
      headers: {
        'Content-Type': 'application/json'
    }}).then((res) =>{
      setTimeout(() => {
        window.location.href = "/natjecanja/"+res.data;
      }, 2500);
    }).catch((error) =>{
      console.log("error occured during post", error)
    });
  }


  return (
    <Box>
      <Typography variant="h5" mt={5} mb={5}>Dobrodošli u aplikaciju za praćenje natjecanja!</Typography>
      <Typography mt={3}>Odaberite vaše natjecanje za administraciju ili kreirajte novo natjecanje.</Typography>

        <Box mt={5}>
          <DataTable
            noDataComponent={<Typography variant="h5">Nemate kreirano niti jedno natjecanje.</Typography>}
            noHeader
            pagination
            paginationPerPage={10}
            progressPending={loading}
            progressComponent={<CircularProgress />}
            paginationRowsPerPageOptions={[5]}
            columns={columns}
            data={records}
            onRowClicked={(row) => {
              window.location.href = "/natjecanja/"+row.id
            }}
          />
        </Box>
       
        <Box mt={2} width={"100%"} display={"flex"} justifyContent={"right"}>
          <Button onClick={handleOpen} variant="contained">Novo natjecanje</Button>
        </Box>
        <Dialog open={openDialog} onClose={handleClose}>
          <DialogTitle color={"primary"}>Novo natjecanje</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Unesite podatke za novo natjecanje
            </DialogContentText>
            <TextField
              required
              autoFocus
              onChange={e => setNaziv(e.target.value)}
              margin="dense"
              id="naziv"
              label="Naziv natjecanja"
              type="text"
              fullWidth
              variant="standard"
              helperText={error.naziv ? "Obavezno polje" : ""}
              error={error.naziv}
            />
            <TextField
              required
              onChange={e => setNatjecatelji(e.target.value)}
              margin="dense"
              id="natjecatelji"
              label="Natjecatelji"
              fullWidth
              variant="outlined"
              helperText={error.natjecatelji ? "Odaberi min 4 i max 8 natjecatelja" : "Jedan red - jedan natjecatelj"}
              multiline
              rows={8}
              error={error.natjecatelji}
            />
            <FormLabel>Način bodovanja</FormLabel>
            <br></br>
            <TextField
              required
              onChange={e => setNacinBodovanja({pobjeda:parseInt(e.target.value), remi:nacinBodovanja.remi, poraz: nacinBodovanja.poraz})}
              margin="dense"
              id="pobjeda"
              label="Pobjeda"
              variant="outlined"
              type="number"
              sx={{width: 150, mr:3}}
              helperText={error.nacinBodovanja.pobjeda ? "Obavezno polje" : ""}
              error={error.nacinBodovanja.pobjeda}
            />
            <TextField
              required
              onChange={e => setNacinBodovanja({pobjeda:nacinBodovanja.pobjeda, remi:parseInt(e.target.value), poraz: nacinBodovanja.poraz})}
              margin="dense"
              id="remi"
              label="Remi"
              variant="outlined"
              type="number"
              sx={{width: 150, mr:3}}
              helperText={error.nacinBodovanja.remi ? "Obavezno polje" : ""}
              error={error.nacinBodovanja.remi}
            />
            <TextField
              required
              onChange={e => setNacinBodovanja({pobjeda:nacinBodovanja.pobjeda, remi:nacinBodovanja.remi, poraz: parseInt(e.target.value)})}
              margin="dense"
              id="poraz"
              label="Poraz"
              variant="outlined"
              type="number"
              sx={{width: 150}}
              helperText={error.nacinBodovanja.poraz ? "Obavezno polje" : ""}
              error={error.nacinBodovanja.poraz}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Odustani</Button>
            <Button onClick={handleConfirm}>Potvrdi</Button>
          </DialogActions>
        </Dialog>
    </Box>
  )
}
