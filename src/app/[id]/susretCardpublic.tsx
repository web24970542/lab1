import {Box, Button, Card, Input, Typography} from '@mui/material';
import { useState } from 'react';


type RowDataSusret = {
    id: number;
    kolo: number;
    natjecatelj_1: string;
    natjecatelj_2: string;
    rezultat_1: number;
    rezultat_2: number;
    odigrano: boolean;
}

const SusretCard = (props: {susret: RowDataSusret, refreshPlasmani: () => void}) => {

    const [rez1, setRez1] = useState(props.susret.rezultat_1);
    const [rez2, setRez2] = useState(props.susret.rezultat_2);

    const [odigran,setOdigran] = useState(props.susret.odigrano);

    const { susret} = props;

    return (

        <Card sx={{bgcolor: "#eeeeee", margin: 2, padding: 2}} >
            <h3>{susret.natjecatelj_1}</h3> vs <h3>{susret.natjecatelj_2}</h3>
            <Box display={"flex"} flexDirection={"row"}>
                <Input type="text" value={rez1}
                  sx={{
                    bgcolor: "#dddddd",
                    px:2
                }}
                />
                <Typography variant='h4' mx={2}>:</Typography>
                <Input type="text" value={rez2}
                  sx={{
                    bgcolor: "#dddddd",
                    px:2
                }}
                />
            </Box>
            {odigran ? <Typography color={"#42c728"} variant='body1'>Susret odigran</Typography> : <Typography color={"#536ffc"} variant='body1'>Susret nije još odigran</Typography>}

        </Card>
    );
        
}

export default SusretCard;
