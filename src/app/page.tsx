"use client"
import { Box, Typography, CircularProgress } from "@mui/material";

import { useUser } from "@auth0/nextjs-auth0/client";
import { Suspense } from "react";
import Loading from "./loading";

export default function Home() {

  const {user,error,isLoading} = useUser();
  
  if (user) {
    window.location.href = "/natjecanja"
  }

  return (
    isLoading ?
      <Box width={"100%"} display={"flex"} justifyContent={"center"}>
        <CircularProgress sx={{mt:5}}/>
      </Box>
      : !user ?
      <Box>
       <Typography variant="h3" mt={5} mb={5} textAlign={"center"}>Dobrodošli na stranicu za upravljanje natjecanjima!</Typography>
        <Typography variant="h6" mt={5} mb={5} textAlign={"center"}>Prijavite se da biste nastavili</Typography>
      </Box>
    : <></>
  )
}
